# Introduction à Docker


## Accéder au REPL Python

* Charger le fichier `python-3.8-slim.tar` sur votre ordinateur

* Accéder en ligne de commande à l'endroit où se situe le fichier et exécuter :

```bash
$ docker load -i python-3.8-slim.tar
```

* Vérifier que l'import a bien été réalisé en exécutant :

```bash
$ docker images
```

* Lancer une invit de commande python : 

```bash
$ docker run -it --rm python:3.8-slim 
```

L'option `-it` indique que nous voulons une session interactive avec le conteneur créé.

L'option `--rm` indique qu'à la fin de notre session le conteneur sera supprimé.

Par défaut nous nous retrouvons dans le __REPL__ de python, une boucle d'évaluation de code python.

```bash
Python 3.8.2 (default, Feb 26 2020, 15:09:34) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

* Ouvrir une autre fenêtre de terminal et exécuter :

```bash
$ docker ps
```

Vous constaterez que le conteneur en cours d'exécution s'est vu attribué un `CONTAINER_ID` identifiant unique du conteneur, ainsi qu'un nom référencé dans `NAMES`, un format plus lisible pour les humains.

* Dans cette même fenêtre, exécuter :

```bash
$ docker logs CONTAINER_ID
```

Puis :

```bash
$ docker logs NAMES
```

Vous devez voir tout ce qui a été affiché par votre conteneur.

Saisissez :

```bash
$ docker logs -f CONTAINER_ID
```

A présent l'invit de commande vous affichera tous les nouveaux logs du conteneur.

Passer à la fenêtre où s'exécute le conteneur et entrez quelques commandes python.

Vérifier que l'interaction a été affichée dans l'autre fenêtre.

* Quitter l'exécution du conteneur :

`ctrl+d` dans la fenêtre du REPL.

Revenez à la fenêtre qui affichait vos logs et observez que le conteneurs n'est plus présent en exécutant :

```bash
$ docker ps -a 
```

## Faire tourner un conteneur python

Maintenant, exécuter :

```bash
$ docker run -it python:3.8-slim /bin/bash
```

Vous vous retrouvez sur une invite de commande.

Suivre la séquence d'instructions suivante (ee393e8d0518 doit correspondre à l'identifiant de votre conteneur) :

```bash
root@ee393e8d0518:/# pwd
/
root@ee393e8d0518:/# ls
bin  boot  dev	etc  home  lib	lib64  media  mnt  opt	proc  root  run  sbin  srv  sys  tmp  usr  var
root@ee393e8d0518:/# cd home/
root@ee393e8d0518:/home# ls
root@ee393e8d0518:/home# cd ..
root@ee393e8d0518:/# cd srv/
root@ee393e8d0518:/srv# ls
root@ee393e8d0518:/srv# touch hello.txt
root@ee393e8d0518:/srv# ls
hello.txt
root@ee393e8d0518:/srv# python
Python 3.8.2 (default, Feb 26 2020, 15:09:34) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> exit
Use exit() or Ctrl-D (i.e. EOF) to exit
>>> 
root@ee393e8d0518:/srv# exit
exit
```

Dans l'exercice précédent, le conteneur était programmé pour exécuter immédiatement la commande python à son lancement. Ici nous l'avons paramétrer pour ouvrir un shell bash via la commande run.

```bash
$ docker ps # n'affiche pas notre conteneur car il n'est plus actif
$ docker ps -a # affiche notre conteneur 
```


Nous pouvons faire en sorte de laisser le conteneur tourner sans y être connecté, après avoir noté l'idenfiant de votre conteneur depuis le résultat de l'exécution précédente :


```bash
$ docker start CONTAINER_ID
$ docker exec -it CONTAINER_ID /bin/bash # ouvre une session interactive avec le conteneur en cours d'exécution
$ docker ps # le conteneur doit apparaître comme `up`
$ docker stop CONTAINER_ID
$ docker ps # le conteneur ne doit plus apparaître
$ docker ps -a # il doit s'afficher ici
```

Maintenant nous pouvons supprimer le conteneur :

```bash
$ docker rm CONTAINER_ID
$ docker ps -a # il n'est plus listé car supprimé du système
```


## Créer un conteneur python lié à notre système de fichier

En vous positionnant via votre terminal dans le dossier que vous voulez mettre à disposition de votre conteneur docker.

Créer un fichier `hello.py` ayant pour code :

```python
import os

print("Hello World!")
cwd = os.getcwd()
print("Je suis exécuté depuis {}".format(cwd))
```

Instruction pour les systèmes UNIX :

```bash
$ pwd # vérifiez que vous êtes dans le bon dossier
$ docker run -it --mount type=bind,src=`pwd`,dst=/srv/local python:3.8-slim /bin/bash 
root@4892bb84669e:/# ls /srv/local # vous devez voir apparaître le contenu du dossier dont le fichier hello.py
root@4892bb84669e:/# cd /srv/local
root@4892bb84669e:/srv/local# python hello.py 
Hello World!
Je suis exécuté depuis /srv/local
```

Modifier le script python en local et exécuter à nouveau le script, consultez également son contenu avec la commande `cat`.


## Créer un conteneur MariaDB 

Nous allons maintenant créer un conteneur qui permettra d'avoir une base de données MariaDB.

```bash
$ docker pull mariadb:10.5.1-bionic # si vous avez de la bande passante
$ docker load -i mariadb-10.5.1-bionic.tar # sinon
$ docker run --name instance_maria -p 3306:3306 -e MYSQL_ROOT_PASSWORD=dirtysecret -d mariadb:10.5.1-bionic
```

`-p` permet d'exposer le port ouvert sur le conteneur sur la machine hôte

`-e` sépcifie une variable d'envionnement 

`-d` lance le conteneur en "daemon", c'est à dire en arrière plan

```bash
$ docker exec -it instance_maria bash
root@b486b59a35a1:/# mysql -u root -pdirtysecret
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.4.12-MariaDB-1:10.4.12+maria~bionic mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
3 rows in set (0.001 sec)

MariaDB [(none)]> 
```

Créer une base de données nommée exemple et une table article.

```sql
CREATE DATABASE exemple;
USE exemple;
CREATE TABLE article
( 
   id INT NOT NULL AUTO_INCREMENT,
   titre VARCHAR(100) NOT NULL,
   contenu TEXT NOT NULL,
   date_creation DATE,
   PRIMARY KEY ( id )
);
SHOW TABLES;
```

Quitter la session interactive avec le conteneur et exécuter la commande suivante :

```bash
$ docker exec instance_maria  mysqldump -u root -pdirtysecret exemple  
```

Sur votre terminal s'affiche un script de backup de la base exemple. Vous pouvez récupérer son résultat dans un fichier nommé `mariabkup.sql` par exemple :

```bash
$ docker exec instance_maria  mysqldump -u root -pdirtysecret exemple > mariabkup.sql
```

Avec l'outil de votre choix, connectez vous à la base de données et insérer / afficher des enregistrements dans la table article.







