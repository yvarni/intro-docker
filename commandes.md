# Commandes utiles


## Lister les conteneurs en cours d'exécution


```bash
docker ps

docker ps -a # y compris les conteneurs inactifs
```

## Lister les images installées en local par docker

```bash
docker images
docker images python # affiche les images python
```

## Créer un conteneur depuis une image

__RUN__ et non `create` destiné à la création d'image. 

```bash
docker run -it [--name NOMCONTENEUR] IMAGE COMMANDE
```

## Ouvrir une console bash avec un conteneur en cours d'exécution 

```bash
docker exec -it CONTAINER_ID /bin/bash
```


## Télécharger une image en local

```bash
docker pull IDENTIFIANT-IMAGE
```

## Afficher les logs d'un conteneur

```bash
docker logs CONTAINER_ID
docker logs -f CONTAINER_ID # affichage "live"
```

## Sauvegarder / charger une image docker dans / depuis un fichier

```bash
docker save IDENTIFIANT-IMAGE -o FICHIER_ARCHIVE
docker import -i FICHIER_ARCHIVE
```



